const map = {
  'working': 0,
  'listening': {'name':'', 'time': 0, 'artist': ''},
  'lastSender': ''
}
const url = 'https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=5DAD723252C842E593C878C79EFE179F&steamids=76561199109983986'

export default async function handler(req, res) {

  // 接受心跳请求
  if (req.method === 'POST') {
    if ('from' in req.body) {
      map.lastSender = req.body.from
    }
    // 工作推送
    if ('working' in req.body) {
      map.working = new Date()
    } else {
      map.working = 0
    }

    // 音乐推送
    if ('musicName' in req.body && req.body.musicName.trim() != '') {
      map.listening.name = req.body.musicName
      map.listening.artist = req.body.musicArtist
      map.listening.time = new Date()
    } else {
      map.listening.name = ''
      map.listening.time = 0
    }

    console.debug(map)
    res.status(200)
    res.send({})
    return
  }

  // 返回的root对象
  const ret = {}

  // 请求 Steam Api
  try {
    // https://developer.valvesoftware.com/wiki/Steam_Web_API#GetPlayerSummaries_.28v0002.29
    const result = await fetch(url)
    const data = await result.json();
    const players = data['response']['players'][0]
    ret['personastate'] = players['personastate']
    ret['gameextrainfo'] = players['gameextrainfo']
    ret['gameid'] = players['gameid']
  } catch (err) {
    console.error(err)
  }

  const now = new Date()
  // working - heartbeat
  const w_heartbeat = (now - map.working) / 1000 / 60
  if (map.working == 0 || w_heartbeat > 2.5) {
    delete ret['working']
    delete ret['w_heartbeat']
  } else {
    ret['working'] = true
    ret['w_heartbeat'] = w_heartbeat
  }

  // music - heartbeat
  const m_heartbeat = (now - map.listening.time) / 1000 / 60
  if (map.listening.time == 0 || m_heartbeat > 2.5) {
    delete ret['listening_music']
    delete ret['listening_artist']
    delete ret['m_heartbeat']
  } else {
    ret['listening_music'] = map.listening.name
    ret['listening_artist'] = map.listening.artist
    ret['m_heartbeat'] = m_heartbeat
  }

  ret['lastSender'] = map.lastSender

  res.status(200)
  res.send(ret)
}